// © Alfredo Correa 2018-2024

#include "../../mpi3/main.hpp"
#include "../../mpi3/communicator.hpp"

#include <cassert>
#include <iterator>
#include <vector>

namespace mpi3 = boost::mpi3;

auto mpi3::main(int/*argc*/, char**/*argv*/, mpi3::communicator world) -> int try {
	using T = double;
	assert( world.size() > 2 );

// initialize local data ///////////////////////////////////////////////////////
	auto v_loc = [&]{
		switch(world.rank()){
			case 0: return std::vector<T>{0.0, 0.0, 0.0}          ;
			case 1: return std::vector<T>{1.0, 1.0, 1.0, 1.0}     ;
			case 2: return std::vector<T>{2.0, 2.0, 2.0, 2.0, 2.0};
			default: return std::vector<T>{};
		}
	}();

// gather communication ////////////////////////////////////////////////////////
	std::vector<T> v;
//  v.reserve(v_local.size()*world.size()); // optional! avoid a few allocations
	world.all_gatherv(begin(v_loc), end(v_loc), std::back_inserter(v)); 
//  v.shrink_to_fit();                      // optional! save a few memory

// check communication /////////////////////////////////////////////////////////
	assert((v==std::vector<T>{0., 0., 0., 1., 1., 1., 1., 2., 2., 2., 2., 2.}));
	return 0;
}catch(...) {return 1;}

