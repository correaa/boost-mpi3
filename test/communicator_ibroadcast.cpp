// Copyright 2020-2024 Alfredo A. Correa

#include <mpi3/communicator.hpp>
#include <mpi3/main.hpp>

#if !defined(EXAMPI)
#include <mpi3/ostream.hpp>
#endif

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <numeric>
#include <thread>
#include <vector>

namespace mpi3 = boost::mpi3;

auto mpi3::main(int /*argc*/, char** /*argv*/, mpi3::communicator world) -> int try {
	assert(world.size() > 2);

	std::vector<int> large(10);
	if(world.root()) {
		iota(large.begin(), large.end(), 0);
	}

#if !defined(EXAMPI)
	mpi3::ostream wout(world);
	wout << "before:\n" << std::flush;
	std::copy(large.begin(), large.end(), std::ostream_iterator<int>(wout, " "));

	wout << '\n' << std::flush;

	{
		auto req = world.ibroadcast(large.begin(), large.end(), 0);
		using namespace std::chrono_literals;
		std::this_thread::sleep_for(5s);  // NOLINT(misc-include-cleaner) bug in clang-tidy 18.1
	}

	wout << "after:\n" << std::flush;
	std::copy(large.begin(), large.end(), std::ostream_iterator<int>(wout, " "));
	wout << '\n' << std::flush;
#endif

	return 0;
} catch(...) {return 1;}
